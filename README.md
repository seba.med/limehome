# Running the apps
This is a guide on how to run `backend` and `frontend` applications.

### Setup 
Before starting the apps, we need to create a database.

For that, we run docker container containing POSTGRES image.

First, we go to the root of the projects and run:
`docker-compose up -d`

After that, docker will create our database, and automatically run init.sql, located in `/db/` folder.

## Note
Before starting any of the projects, once we navigate to their folder, we need to run this command:
`npm install`

### Backend
Once we have created the database, we need to run the backend application.

Navigate into the `/backend/` folder and run of the following commands for:
- running all tests - `npm run test`
- running the app in development mode - `npm run dev`

If we run the app in development mode, it will be hosted on http://localhost:3000 by default. 

**Swagger** documentation can be accessed on http://localhost:3000/docs/swagger

### Frontend
Now, we are ready to start the frontend application.

Navigate to `/frontend/` folder and run:
`ng serve -o`

It may be necessary to install the *Angular CLI* via `npm i -g @angular/cli`!

The browser should immediately be populated with a new tab, our app, that will be hosted on http://localhost:4200

The app contains features:
- Show nearby properties
- Click on a property to show detailed view
- Go to the new location by typing it in input field on the top of the app
- Smooth animations
- Load new properties on map drag end

### Done
And, we are done!

### Docker
Project is dockerized, so we can run it as a whole using only one command:
`docker-compose up --build -d`

After docker has done it's job, applications will be available on:
- frontend - http://localhost:4200
- backend - http://localhost:3000 (add /docs/swagger for swagger documentation)

### Application is also deployed to Digital Ocean
Find it at http://46.101.175.43:4200


