const bookingRoutes = require('./booking.routes');
const propertyRoutes = require('./property.routes');
const swaggerRoutes = require('./swagger/swagger.routes');

module.exports = {
    bookingRoutes,
    propertyRoutes,
    swaggerRoutes
}