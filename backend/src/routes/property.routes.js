const express = require('express');

const router = express.Router();

const { getAllFromHereApiByAtHandler, getAllByPropertyIdHandler } = require('../controllers/property.controller');

// Get properties at lat,lng
// Expects `at` query param
router.get('/api/properties', getAllFromHereApiByAtHandler);

// Get bookings for a property by property id
// Expects property_id in url
router.get('/api/properties/:property_id/bookings', getAllByPropertyIdHandler);

module.exports = router;