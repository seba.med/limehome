const express = require('express');

const router = express.Router();

const { createBookingHandler } = require('../controllers/booking.controller');

router.post('/api/bookings', createBookingHandler);

module.exports = router;