const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = process.env.NODE_ENV === 'development' ? require('./swagger.json') : require('./swagger-prod.json');

router.use('/docs/swagger', swaggerUi.serve);
router.get('/docs/swagger', swaggerUi.setup(swaggerDocument));

module.exports = router;