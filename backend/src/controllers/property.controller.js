const { getAllFromHereApiByAt, getAllByPropertyId } = require('../services/property.service');

const getAllFromHereApiByAtHandler = async (req, res) => {
    try {
        res.status(200).send(await getAllFromHereApiByAt(req.query.at))
    } catch (e) {
        res.status(400).send(e.message);
    }
}

const getAllByPropertyIdHandler = async (req, res) => {
    try {
        res.status(200).send(await getAllByPropertyId(req.params.property_id))
    } catch (e) {
        res.status(400).send(e.message);
    }
}

module.exports = {
    getAllFromHereApiByAtHandler,
    getAllByPropertyIdHandler
}