// promeniti i u importu 
const { createBooking } = require('../services/booking.service');

const createBookingHandler = async (req, res) => {
    try {
        await createBooking(req.body);
        res.status(201).send('Successfully created!');
    } catch (e) {
        res.status(400).send(e);
    }
}

module.exports = {
    createBookingHandler
}