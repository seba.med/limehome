const HERE_API_URL = at => `https://places.ls.hereapi.com/places/v1/autosuggest?apiKey=${process.env.HERE_API_KEY}&at=${at}&q=hotel`;

module.exports = {
    HERE_API_URL
}
