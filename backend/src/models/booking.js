module.exports = (sequelize, DataTypes) => {

    var Booking = sequelize.define('booking', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        property_id: DataTypes.STRING,
        check_in: DataTypes.DATE,
        check_out: DataTypes.DATE
    }, {
        timestamps: false
    });

    return Booking;
};