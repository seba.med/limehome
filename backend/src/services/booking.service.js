const models = require('../models')

const createBooking = async requestDTO => {
    const { property_id, check_in, check_out } = requestDTO;

    try {
        await models.booking.create({
            property_id,
            check_in,
            check_out
        });
    } catch (e) {
        throw new Error('Entity cannot be saved!');
    }
}

module.exports = {
    createBooking
}