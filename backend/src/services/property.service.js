const axios = require('axios');
const models = require('../models');

const { HERE_API_URL } = require('../utils/consts');

const getAllFromHereApiByAt = (param) => {
    return axios.get(HERE_API_URL(param))
        .then(res => {
            // getting only properties with an id, so we can store that id in the database
            return res.data.results.filter(h => h.id);
        })
        .catch(e => {
            throw new Error('Failed fetching properties!');
        })
}

const getAllByPropertyId = async property_id => {
    try {

        return await models.booking.findAll({
            where: {
                property_id
            },
            raw: true
        })
    } catch (e) {
        throw new Error('Failed fetching bookings from the database!');
    }
}

module.exports = {
    getAllFromHereApiByAt,
    getAllByPropertyId
}