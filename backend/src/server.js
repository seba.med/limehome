const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');

const envPath = path.resolve(process.cwd(), '', '.env');
require('dotenv').config({
    path: envPath,
});

const app = express();
const port = process.env.APP_PORT || 3000;

const { bookingRoutes, propertyRoutes, swaggerRoutes } = require('./routes');

app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json());

app.use([bookingRoutes, propertyRoutes, swaggerRoutes]);

app.listen(port, () => console.log(`App started successfully! Try it at http://localhost:${port}`));

module.exports = app;