const request = require('supertest');
const app = require('../src/server');
const models = require('../src/models');

const property_id = 'd8989aw-d98ad1-a1dfv-awd182djn1';

afterAll(async () => {
    await models.sequelize.query(`DELETE FROM bookings WHERE property_id = '${property_id}'`);
})


describe('/api/bookings - POST', () => {
    it('should return a response with status code 201 and create a new instance of Booking entity', async () => {
        const res = await request(app)
            .post('/api/bookings')
            .send({
                property_id: property_id,
                check_in: new Date(),
                check_out: new Date()
            });


        expect(res.statusCode).toEqual(201);
    });

    it('should return a response with status code 400', async () => {
        const res = await request(app)
            .post('/api/bookings')
            .send({
                check_in: new Date(),
                check_out: new Date()
            });


        expect(res.statusCode).toEqual(400);
    });
});