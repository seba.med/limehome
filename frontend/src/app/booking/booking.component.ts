import { Component, OnDestroy } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { Subscription } from 'rxjs';
import { PropertyService } from '../services/property/property.service';
import { BookingService } from '../services/booking/booking.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':enter', [
        style({ transform: 'translateY(100%)', opacity: 0 }),
        animate('300ms ease-in', style({ transform: 'translateY(0)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0)', opacity: 1 }),
        animate('300ms ease-in', style({ transform: 'translateY(100%)', opacity: 0 }))
      ])
    ]
    )
  ],
})
export class BookingComponent implements OnDestroy {

  property: any = null;

  subscription: Subscription;

  constructor(private propertyService: PropertyService, private bookingService: BookingService) {
    this.subscription = this.propertyService.getSelectedProperty().subscribe(property => {
      this.property = property;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  handleClose() {
    this.propertyService.removeSelectedProperty();
  }

  handleBooking(property) {
    this.bookingService.createBooking(property.id).subscribe(
      res => {
        // couldbedone: add toast or confirm message
      },
      error => {
        // couldbedone: add toast or message
      }
    );
  }

}
