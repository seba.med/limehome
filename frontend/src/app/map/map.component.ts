import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Property } from '../models/property';
import { PropertyService } from '../services/property/property.service';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  selectedProperty: Property = null;

  iconUrl = {
    url: '../../assets/icons/marker.png',
    scaledSize: {
      width: 40,
      height: 40
    }
  };

  selectedIconUrl = {
    ...this.iconUrl,
    url: '../../assets/icons/selected-marker.png'
  };

  latitude = 45.2671;
  longitude = 19.8335;

  properties: Property[] = [];

  googleInputOptions = {
    types: []
  };

  @ViewChild('placesRef') placesRef: GooglePlaceDirective;

  constructor(private propertyService: PropertyService) {
    this.subscription = this.propertyService.getSelectedProperty().subscribe(property => {
      this.selectedProperty = property;
    });
  }

  ngOnInit(): void {
    this.setPlacesOnMap(this.latitude, this.longitude);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setPlacesOnMap(lat: number, lng: number): void {
    this.propertyService.getPropertiesByLocation(`${lat},${lng}`).subscribe(
      (res: Property[]) => {
        this.properties = [...this.properties, ...res.filter(r => this.properties.findIndex(p => p.id === r.id) === -1)];
      }
    );
  }

  public handleMarkerClicked(property: Property) {
    this.propertyService.setSelectedProperty(property);
    this.latitude = property.position[0];
    this.longitude = property.position[1];
  }

  public handleAddressChange(address: Address) {
    this.latitude = address.geometry.location.lat();
    this.longitude = address.geometry.location.lng();
    this.setPlacesOnMap(this.latitude, this.longitude);
  }

  public mapReady(map) {
    map.addListener('dragend', () => {
      const lat = map.center.lat();
      const lng = map.center.lng();
      const DISTANCE_FACTOR = 0.05;

      if (Math.abs(lat - this.latitude) > DISTANCE_FACTOR || Math.abs(lng - this.longitude) > DISTANCE_FACTOR) {
        this.setPlacesOnMap(lat, lng);
      }
    });
  }

}
