export class CreateBookingDTO {
  property_id: string;
  check_in: Date;
  check_out: Date;
}
