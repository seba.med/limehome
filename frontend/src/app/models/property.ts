export class Property {
  category: string;
  categoryTitle: string;
  distance: number;
  highlightedTitle: string;
  highlightedVicinity: string;
  href: string;
  id: string;
  position: number[];
  resultType: string;
  title: string;
  type: string;
  vicinity: string;
}
