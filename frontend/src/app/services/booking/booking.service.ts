import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CreateBookingDTO } from 'src/app/models/booking';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  API_URL = `${environment.backendUrl}/bookings`;

  constructor(private http: HttpClient) { }

  createBooking(propertyId: string) {
    const body: CreateBookingDTO = {
      property_id: propertyId,
      check_in: new Date(),
      check_out: new Date()
    };

    return this.http.post(this.API_URL, body);
  }

}
