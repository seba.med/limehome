import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { Property } from 'src/app/models/property';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  API_URL = `${environment.backendUrl}/properties`;

  private selectedProperty = new Subject<Property>();

  constructor(private http: HttpClient) { }

  getPropertiesByLocation(at): Observable<Property[]> {
    return this.http.get<Property[]>(`${this.API_URL}?at=${at}`);
  }

  setSelectedProperty(property: Property): void {
    this.selectedProperty.next(property);
  }

  removeSelectedProperty(): void {
    this.selectedProperty.next(null);
  }

  getSelectedProperty(): Observable<Property> {
    return this.selectedProperty.asObservable();
  }
}
