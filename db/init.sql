-- psql -h localhost -p 5432 -U user limehome

CREATE TABLE IF NOT EXISTS Bookings (
   id SERIAL PRIMARY KEY,
   property_id varchar not null,
   check_in date not null,
   check_out date not null
);